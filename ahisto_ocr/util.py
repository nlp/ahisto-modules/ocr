from contextlib import contextmanager
import csv
from logging import getLogger
import tarfile
from tarfile import TarFile, TarInfo
from tempfile import TemporaryFile
from time import time
from typing import Iterator
from contextlib import closing
from io import BytesIO, TextIOWrapper
import os
from subprocess import check_output
import sys
from pathlib import Path

from .config import CONFIG as _CONFIG

from docker.errors import ContainerError


LOGGER = getLogger(__name__)
CONFIG = _CONFIG['util']


def detect_free_gpu() -> str:

    def to_mebibytes(human_readable_amount: str) -> int:
        amount, unit = human_readable_amount.split()
        assert unit == 'MiB'
        mebibytes = int(amount)
        return mebibytes

    command = ['nvidia-smi', '--format=csv,noheader', '--query-gpu=memory.free,memory.total,name,serial,index']
    output = TextIOWrapper(BytesIO(check_output(command)))
    lines = [
        (to_mebibytes(memory_free), to_mebibytes(memory_total), name.strip(), serial, int(index))
        for memory_free, memory_total, name, serial, index
        in csv.reader(output)
        if to_mebibytes(memory_total) <= to_mebibytes(CONFIG['auto_gpu_maximum_total_memory'])
    ]
    if not lines:
        raise ValueError('Cannot detect any appropriate GPUs on the system')

    memory_free, _, name, __, index = max(lines)
    LOGGER.info(f'Automatically selected "{name}" NVIDIA GPU (PCI BUS ID: {index}, {memory_free}M free VRAM)')

    return str(index)


@contextmanager
def create_temporary_docker_volume(client, *args, **kwargs) -> Iterator:
    volume = client.volumes.create(*args, **kwargs)
    LOGGER.debug(f'Created temporary Docker volume {volume.name}')
    try:
        yield volume
    finally:
        volume.remove(force=True)
        LOGGER.debug(f'Deleted temporary Docker volume {volume.name}')


@contextmanager
def create_temporary_docker_container(client, *args, **kwargs) -> Iterator:
    container = client.containers.create(*args, **kwargs)
    LOGGER.debug(f'Created temporary Docker container {container.short_id}')
    try:
        yield container
    finally:
        container.remove(force=True)
        LOGGER.debug(f'Deleted temporary Docker container {container.short_id}')


def run_docker_container(client, *args, quiet: bool = True, **kwargs) -> None:
    kwargs = {**{'remove': True, 'stdout': True, 'stderr': True}, **kwargs}
    try:
        output = client.containers.run(*args, **kwargs)
        if not quiet:
            with os.fdopen(sys.stdout.fileno(), 'wb', closefd=False) as stdout:
                stdout.write(output)
    except ContainerError as e:
        e.container.remove(force=True)
        raise e


def add_text_file_to_tarfile(tar: TarFile, filename: str, text: str) -> None:
    with closing(BytesIO(text.encode('utf-8'))) as f:
        tarinfo = TarInfo(filename)
        tarinfo.size = len(f.getvalue())
        tarinfo.mtime = time()
        tar.addfile(tarinfo, fileobj=f)


def add_text_file_to_container(container, filename: str, text: str) -> None:
    with TemporaryFile('w+b') as tf:
        with tarfile.open(fileobj=tf, mode='w') as tar:
            add_text_file_to_tarfile(tar, filename, text)
        tf.seek(0)
        container.put_archive(path='/', data=tf)


def extract_text_file_from_tarfile(tar: TarFile, filename: str) -> str:
    data = tar.extractfile(filename)
    assert data is not None
    text = data.read().decode('utf-8')
    return text


def extract_text_file_from_container(container, filename: str) -> str:
    with TemporaryFile('w+b') as tf:
        bits, _ = container.get_archive(filename)
        for chunk in bits:
            tf.write(chunk)
        tf.seek(0)
        with tarfile.open(fileobj=tf, mode='r') as tar:
            text = extract_text_file_from_tarfile(tar, Path(filename).name)
    return text
