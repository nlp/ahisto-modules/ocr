import logging
from logging import getLogger
from typing import List
from pathlib import Path

from .config import CONFIG as _CONFIG
from .util import create_temporary_docker_volume, detect_free_gpu
from .volume import copy_input_to, copy_output_from
from .preprocessing import apply_super_resolution
from .ocr import run_tesseract, run_pero_ocr, combine_results

import click
import docker


LOGGER = getLogger(__name__)
CONFIG = _CONFIG['cli']


@click.command()
@click.argument('input-dir',
                type=click.Path(exists=True, dir_okay=True, file_okay=False),
                required=True)
@click.argument('output-dir',
                type=click.Path(exists=None, dir_okay=True, file_okay=False),
                required=True)
@click.option('--pero-ocr/--no-pero-ocr',
              help='Use PERO in addition to Tesseract for OCR (recommended, requires GPU)',
              default=bool(CONFIG.getint('pero_ocr')),
              is_flag=True,
              required=False)
@click.option('--super-resolution/--no-super-resolution',
              help='Use image super-resolution before the OCR (recommended, requires GPU)',
              default=bool(CONFIG.getint('super_resolution')),
              is_flag=True,
              required=False)
@click.option('--gpus',
              help=('Either comma-separated PCI BUS IDs of NVIDIA GPUs that will be used for image super-resolution '
                    'and PERO OCR, `auto` for an automatic selection of a free GPU, or `all` for the use of all GPUs'),
              default=CONFIG['gpus'],
              required=False)
def main(input_dir: str, output_dir: str, pero_ocr: bool, super_resolution: bool, gpus: str) -> None:

    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

    client = docker.from_env()

    def check_docker_image(name: str) -> None:
        if not client.images.list(name):
            LOGGER.info(f'Pulling image {name}')
            client.images.pull(name)

    check_docker_image('gitlab.fi.muni.cz:5050/nlp/ahisto-modules/empty')
    check_docker_image('gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-eval')
    check_docker_image('gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-fileformat')
    check_docker_image('gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-tesseract')
    check_docker_image('gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-waifu2x')
    check_docker_image('gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-pero')

    input_dir = Path(input_dir)
    image_suffixes = {'.jpg', '.jpeg', '.jfif', '.png', '.tif', '.tiff'}
    input_images = sorted(
        filename.relative_to(input_dir)
        for filename
        in input_dir.glob('**/*.*')
        if filename.is_file() and filename.suffix.lower() in image_suffixes
    )

    if not input_images:
        LOGGER.info(f'No input images found in {input_dir}')
    else:
        output_dir = Path(output_dir)

        run_ocr(input_dir, input_images, output_dir, pero_ocr, super_resolution, gpus)


def run_ocr(input_dir: Path, input_images: List[Path], output_dir: Path,
            pero_ocr: bool, super_resolution: bool, gpus: str) -> None:
    client = docker.from_env()
    with create_temporary_docker_volume(client) as input_volume, \
            create_temporary_docker_volume(client) as postprocessing_volume, \
            create_temporary_docker_volume(client) as tesseract_volume, \
            create_temporary_docker_volume(client) as pero_ocr_volume, \
            create_temporary_docker_volume(client) as output_volume:

        copy_input_to(client, input_volume, input_dir, input_images)

        if super_resolution:
            actual_gpus = detect_free_gpu() if gpus == 'auto' else gpus
            apply_super_resolution(client, input_volume, postprocessing_volume, actual_gpus)
        else:
            LOGGER.info('Skipping image super-resolution')
            postprocessing_volume = input_volume

        run_tesseract(client, postprocessing_volume, tesseract_volume)

        if pero_ocr:
            actual_gpus = detect_free_gpu() if gpus == 'auto' else gpus
            run_pero_ocr(client, input_volume, pero_ocr_volume, actual_gpus)
            combine_results(client, tesseract_volume, pero_ocr_volume, output_volume)
        else:
            LOGGER.info('Skipping PERO OCR')
            output_volume = tesseract_volume

        copy_output_from(client, output_volume, input_images, output_dir)
