from logging import getLogger
from pathlib import Path

from .config import CONFIG as _CONFIG
from .util import (
    create_temporary_docker_container,
    extract_text_file_from_container,
    add_text_file_to_container,
    run_docker_container,
)


LOGGER = getLogger(__name__)
CONFIG = _CONFIG['preprocessing']


def apply_super_resolution(client, input_volume, postprocessing_volume, gpus: str) -> None:
    LOGGER.info('Pre-processing the input images using super-resolution')

    volumes = {
        input_volume.name: {
            'bind': '/input',
            'mode': 'ro',
        },
        postprocessing_volume.name: {
            'bind': '/output',
            'mode': 'rw',
        },
    }

    command = [
        'parallel', '--halt', 'now,fail=1', '--jobs', '1', '--',
        'cp {} /output/{/}', '::::', '/input/list.txt',
    ]
    run_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-eval', command=command, volumes=volumes)

    with create_temporary_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/empty', command='cmd',
                                           volumes=volumes) as container:
        text = extract_text_file_from_container(container, '/input/list.txt')
        add_text_file_to_container(container, '/output/list.txt', text)

    volumes = {
        postprocessing_volume.name: {
            'bind': '/input',
            'mode': 'rw',
        },
    }

    command = [
        'th', '/root/waifu2x/waifu2x.lua', '-force_cudnn', '1',
        '-model_dir', f'/root/waifu2x/models/{CONFIG["model"]}', '-m', 'noise_scale',
        '-scale', '2', '-noise_level', str(CONFIG.getint('noise_level')), '-l', '/input/list.txt',
    ]
    run_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-waifu2x', runtime='nvidia',
                         environment={'NVIDIA_VISIBLE_DEVICES': gpus},
                         command=command, volumes=volumes)

    with create_temporary_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/empty', command='cmd',
                                           volumes=volumes) as container:
        filenames = [
            '{}_noise_scale.png'.format(Path(filename).with_suffix(''))
            for filename
            in extract_text_file_from_container(container, '/input/list.txt').split('\n')
        ]
        add_text_file_to_container(container, '/input/list.txt', '\n'.join(filenames))
