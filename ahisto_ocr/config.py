from configparser import ConfigParser
from itertools import chain
from logging import getLogger
import os

from xdg.BaseDirectory import load_config_paths


CONFIG = ConfigParser()

RESOURCE_NAME = 'ahisto_ocr'

DEFAULT_CONFIG_FILE_PATHNAME = os.path.join(
    os.path.dirname(__file__),
    'default.ini',
)
SITE_WIDE_CONFIG_FILE_PATHNAME = os.path.join(
    '/nlp/projekty/ahisto/ahisto.git/ocr',
    '{}.ini'.format(RESOURCE_NAME),
)
WORKING_DIRECTORY_CONFIG_FILE_PATHNAME = os.path.join(
    '.',
    '{}.ini'.format(RESOURCE_NAME),
)

CONFIG_FILE_PATHNAMES = list(chain(
    [DEFAULT_CONFIG_FILE_PATHNAME, SITE_WIDE_CONFIG_FILE_PATHNAME],
    list(load_config_paths('{}.ini'.format(RESOURCE_NAME))),
    [WORKING_DIRECTORY_CONFIG_FILE_PATHNAME],
))

LOGGER = getLogger(__name__)


for pathname in CONFIG_FILE_PATHNAMES:
    LOGGER.debug('Reading configuration file {}'.format(pathname))
    try:
        with open(pathname, 'r', encoding='utf8') as f:
            CONFIG.read_file(f)
    except OSError as err:
        LOGGER.debug('Failed to read configuration file {0}: {1}'.format(pathname, err))
