from logging import getLogger

from .config import CONFIG as _CONFIG
from .util import (
    create_temporary_docker_volume,
    create_temporary_docker_container,
    run_docker_container,
    extract_text_file_from_container,
    add_text_file_to_container,
)


LOGGER = getLogger(__name__)
CONFIG = _CONFIG['ocr']


def run_tesseract(client, preprocessing_volume, second_pass_volume) -> None:

    with create_temporary_docker_volume(client) as first_pass_volume:
        run_tesseract_first_pass(client, preprocessing_volume, first_pass_volume)
        run_tesseract_second_pass(client, preprocessing_volume, first_pass_volume, second_pass_volume)

    volumes = {
        preprocessing_volume.name: {
            'bind': '/input',
            'mode': 'ro',
        },
        second_pass_volume.name: {
            'bind': '/output',
            'mode': 'rw',
        },
    }
    with create_temporary_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/empty', command='cmd',
                                           volumes=volumes) as container:
        text = extract_text_file_from_container(container, '/input/list.txt')
        add_text_file_to_container(container, '/output/list.txt', text)


def run_tesseract_first_pass(client, preprocessing_volume, first_pass_volume) -> None:
    LOGGER.info('Running the first pass of Tesseract with all languages')

    volumes = {
        preprocessing_volume.name: {
            'bind': '/input',
            'mode': 'ro',
        },
        first_pass_volume.name: {
            'bind': '/output',
            'mode': 'rw',
        },
    }

    command = [
        'parallel', '--halt', 'now,fail=1', '--jobs', '25%', '--',
        f'nice -n {CONFIG.getint("niceness")} tesseract {{}} /output/{{/.}} -l {CONFIG["languages"]} '
        '--oem 1 --psm 3 --tessdata-dir /tessdata hocr', '::::', '/input/list.txt',
    ]
    run_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-tesseract', command=command, volumes=volumes)

    command = [
        'python', '-m', 'scripts.extract_detected_languages', '/input/list.txt', '/output',
        CONFIG["languages"], '/output/detected-languages.txt', '00',
    ]
    run_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-eval', command=command, volumes=volumes)


def run_tesseract_second_pass(client, preprocessing_volume, first_pass_volume,
                              second_pass_volume) -> None:
    LOGGER.info('Running the second pass of Tesseract with selected languages')

    volumes = {
        preprocessing_volume.name: {
            'bind': '/input',
            'mode': 'ro',
        },
        first_pass_volume.name: {
            'bind': '/first-pass',
            'mode': 'ro',
        },
        second_pass_volume.name: {
            'bind': '/output',
            'mode': 'rw',
        },
    }

    command = [
        'parallel', '--halt', 'now,fail=1', '--jobs', '25%', '-N', '2', '--',
        'if [ {2} = None ]; then echo | tee /output/{1/.}.hocr /output/{1/.}.txt '
        '/output/{1/.}.tsv /output/{1/.}.box /output/{1/.}.page; '
        'echo empty > /output/{1/.}.ground-truth; else '
        f'nice -n {CONFIG.getint("niceness")} tesseract {{1}} /output/{{1/.}} -l {{2}} '
        '--oem 1 --psm 3 --tessdata-dir /tessdata hocr txt tsv makebox; echo tesseract > '
        '/output/{1/.}.ground-truth; fi', '::::', '/first-pass/detected-languages.txt'
    ]
    run_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-tesseract', command=command, volumes=volumes)

    del volumes[first_pass_volume.name]

    command = [
        'parallel', '--halt', 'now,fail=1', '--jobs', '100%', '--',
        f'! grep -q . /output/{{/.}}.hocr || nice -n {CONFIG.getint("niceness")} ocr-transform '
        'hocr page /output/{/.}.hocr /output/{/.}.page', '::::', '/input/list.txt',
    ]
    run_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-fileformat', command=command, volumes=volumes)


def run_pero_ocr(client, preprocessing_volume, pero_ocr_volume, gpus: str) -> None:
    LOGGER.info('Running PERO OCR')

    volumes = {
        preprocessing_volume.name: {
            'bind': '/input',
            'mode': 'ro',
        },
        pero_ocr_volume.name: {
            'bind': '/output',
            'mode': 'rw',
        },
    }

    command = [
        'nice', '-n', f'{CONFIG.getint("niceness")}',
        'python', 'user_scripts/parse_folder.py', '-i', '/input',
        '--output-xml-path', '/output',
        '--output-alto-path', '/output',
        '-c', 'config.ini',
    ]
    run_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-pero', runtime='nvidia',
                         environment={'NVIDIA_VISIBLE_DEVICES': gpus},
                         command=command, volumes=volumes)

    command = [
        'parallel', '--halt', 'now,fail=1', '--jobs', '100%', '--',
        'python ../page-to-text/page_to_text.py /output/{/.}.xml > /output/{/.}.txt', '::::',
        '/input/list.txt',
    ]
    run_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-pero', command=command, volumes=volumes,
                         environment={'LC_ALL': 'C'})

    with create_temporary_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/empty', command='cmd',
                                           volumes=volumes) as container:
        text = extract_text_file_from_container(container, '/input/list.txt')
        add_text_file_to_container(container, '/output/list.txt', text)


def combine_results(client, tesseract_volume, pero_ocr_volume, output_volume):
    LOGGER.info('Combining Tesseract with Pero OCR')

    volumes = {
        tesseract_volume.name: {
            'bind': '/input',
            'mode': 'ro',
        },
        pero_ocr_volume.name: {
            'bind': '/pero-ocr',
            'mode': 'ro',
        },
        output_volume.name: {
            'bind': '/output',
            'mode': 'rw',
        },
    }

    command = [
        'python', '-m', 'scripts.combine_tesseract_with_pero_ocr_docker',
        '/input', '/pero-ocr', '/output',
    ]
    run_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-eval', command=command, volumes=volumes)

    with create_temporary_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/empty', command='cmd',
                                           volumes=volumes) as container:
        text = extract_text_file_from_container(container, '/pero-ocr/list.txt')
        add_text_file_to_container(container, '/output/list.txt', text)
