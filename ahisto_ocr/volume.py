from typing import List
from pathlib import Path
from logging import getLogger
from shutil import copyfileobj
from tempfile import TemporaryFile
import tarfile

from .util import create_temporary_docker_container, add_text_file_to_tarfile, extract_text_file_from_tarfile


LOGGER = getLogger(__name__)


def copy_input_to(client, volume, input_dir: Path, input_images: List[Path]) -> None:
    LOGGER.info(f'Copying {len(input_images)} input images from {input_dir}/ to Docker volume {volume.short_id}')
    volumes = {volume.name: {'bind': '/input', 'mode': 'rw'}}
    with create_temporary_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/empty', command='cmd',
                                           volumes=volumes) as container:
        with TemporaryFile('w+b') as tf:
            with tarfile.open(fileobj=tf, mode='w') as tar:
                filenames = []
                for input_image_number, input_image in enumerate(input_images):
                    input_image = input_dir / input_image
                    filename = f'{input_image_number}{input_image.suffix}'
                    filenames.append(f'/input/{filename}')
                    tar.add(str(input_image.resolve()), filename)
                add_text_file_to_tarfile(tar, 'list.txt', '\n'.join(filenames))
            tf.seek(0)
            container.put_archive(path='/input', data=tf)


def copy_output_from(client, volume, input_images: List[Path], output_dir: Path) -> None:
    LOGGER.info(f'Copying {len(input_images)} OCR texts from Docker volume {volume.short_id} to {output_dir}/')
    volumes = {volume.name: {'bind': '/output', 'mode': 'ro'}}
    with create_temporary_docker_container(client, 'gitlab.fi.muni.cz:5050/nlp/ahisto-modules/empty', command='cmd',
                                           volumes=volumes) as container:
        with TemporaryFile('w+b') as tf:
            for chunk in container.get_archive('/output')[0]:
                tf.write(chunk)
            tf.seek(0)
            with tarfile.open(fileobj=tf, mode='r') as tar:
                filenames = map(Path, extract_text_file_from_tarfile(tar, 'output/list.txt').split('\n'))
                for input_filename, output_filename in zip(filenames, input_images):
                    (output_dir / output_filename.parent).mkdir(exist_ok=True, parents=True)
                    input_filename = Path('output') / input_filename.stem
                    output_filename = (output_dir / output_filename).with_suffix('')
                    for suffix in ('hocr', 'tsv', 'box', 'page', 'txt', 'ground-truth', 'xml', 'alto'):
                        fsrc = tar.extractfile(str(input_filename.with_suffix(f'.{suffix}')))
                        assert fsrc is not None
                        with output_filename.with_suffix(f'.{suffix}').open('wb') as fdst:
                            copyfileobj(fsrc, fdst)
