`ahisto-ocr` is a command-line tool for the optical character recognition in medieval texts, which was developed as a part of [the AHISTO TACR Eta grant project][4], number TL03000365.

## Installation

To install the tool, perform the following steps:

1. [Install Python][4], [Docker][5], and [NVIDIA Container Toolkit][9].
2. [Create and activate a Python virtualenv][6] and run `make install` from the command line to install the tool.

## Usage

To quickly recognize text in all images in directory input/ and save the result
to directory output/:

    ahisto-ocr input/ output/

To achieve the best results, you should also enable image super-resolution
and PERO OCR (require GPU):

    ahisto-ocr --super-resolution --pero-ocr --gpus 10,11,12 input/ output/

Here is example output of the tool for two images:

    2022-08-24 19:03:57,660   Copying 110 input images from input/ to Docker volume eac109062e
    2022-08-24 19:03:59,715   Pre-processing the input images using super-resolution
    2022-08-24 19:29:09,086   Running the first pass of Tesseract with all languages
    2022-08-24 19:45:34,582   Running the second pass of Tesseract with selected languages
    2022-08-24 19:53:11,737   Running PERO OCR
    2022-08-24 20:11:55,569   Combining Tesseract with Pero OCR
    2022-08-24 20:11:58,007   Copying 110 OCR texts from Docker volume 239b1ec33c to output/

Run `ahisto-ocr --help` from the command line for more information.

## More Information {#more-information}

The development of the tool has been documented in the following two conference articles:

- [V Novotný: When Tesseract Does It Alone: Optical Character Recognition of
  Medieval Texts. RASLAN, 2020.][8]
- [V Novotný, K Seidlová, T Vrabcová, A Horák. When Tesseract Brings Friends:
  Layout Analysis, Language Identification, and Super-Resolution for the
  Optical Character Recognition of Medieval Texts. RASLAN, 2021.][3]

Since August 2022 (see commit 7e943d0), the tool uses PERO OCR from the Brno
University of Technology instead of Google Vision AI:

- [O Kodym, M Hradiš: Page Layout Analysis System for Unconstrained Historic
  Documents. ICDAR, 2021.][10]
- [M Kišš, K Beneš, M Hradiš: AT-ST: Self-Training Adaptation Strategy for OCR
  in Domains with Limited Transcriptions. ICDAR, 2021.][11]
- [J Kohút, M Hradiš: TS-Net: OCR Trained to Switch Between Text Transcription
  Styles. ICDAR, 2021.][12]

## Notes

File [`when-tesseract-brings-friends.ipynb`][1] with OCR evaluation experiments
from [the RASLAN 2021 article When Tesseract Brings Friends][3] is available in
[the `ahisto-ocr-eval` repository][2].

 [1]: https://gitlab.fi.muni.cz/xnovot32/ahisto-ocr-eval/-/blob/master/when-tesseract-brings-friends.ipynb
 [2]: https://gitlab.fi.muni.cz/xnovot32/ahisto-ocr-eval
 [3]: https://nlp.fi.muni.cz/raslan/2021/paper10.pdf
 [4]: https://www.python.org/downloads/
 [5]: https://docs.docker.com/engine/install/
 [6]: https://docs.python.org/3/library/venv.html
 [7]: https://starfos.tacr.cz/en/project/TL03000365
 [8]: https://nlp.fi.muni.cz/raslan/2020/paper1.pdf
 [9]: https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker
 [10]: http://doi.org/10.1007/978-3-030-86331-9_32
 [11]: http://doi.org/10.1007/978-3-030-86337-1_31
 [12]: http://doi.org/10.1007/978-3-030-86337-1_32
