This chapter contains the technical documentation of the AHISTO OCR tool and the associated software enclosed in the `zdrojovy_kod.zip` and `docker_obrazy_ocr.zip` archives.

# Design

In our software, we used the object-oriented programming paradigm to separate the implementation into separate modules that correspond to the individual responsibilities identified by requirement analysis. As our programming language, we used Python 3.7 and third-party software libraries documented in files `ocr/requirements.txt` and `ocr-eval/requirements.txt`. To package our software and trained named entity recognition models, we used Docker.

# Architecture

Here are the modules of our tool and their responsibilities, see also Figure <#package-diagram>:

- `ocr.preprocessing`: Apply superresolution to scanned document images. [@bankovic2021application; @novotny2021when]
- `ocr.ocr`: Extract text from scanned document images using Tesseract OCR and PERO OCR. [@novotny2022when]
- `ocr-eval`: Combine results of Tesseract OCR and PERO OCR. [@novotny2022when]

 /package-diagram.tex

Here are the components of our tool and how they interact, see also Figure <#component-diagram>:

- The `ahisto/ocr-eval` Docker image contains the `ocr-eval` module.
  This Docker image is available at `gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-eval`.
- The `ahisto/tesseract` Docker image contains the Tesseract OCR tool.
  This Docker image is available at `gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-tesseract`.
- The `ahisto/pero-ocr` Docker image contains the PERO OCR tool.
  This Docker image is available at `gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-pero`.
- The `ahisto/waifu2x` Docker image contains an image superresolution model.
  This Docker image is available at `gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-waifu2x`.
- The `ahisto/ocr-fileformat` Docker image contains a tool for converting extracted text to different output formats.
  This Docker image is available at `gitlab.fi.muni.cz:5050/nlp/ahisto-modules/ocr-fileformat`.
- The `ocr` module provides a command-line interface to the Docker images.

 /component-diagram.tex
